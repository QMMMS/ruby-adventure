﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class AgainButton : MonoBehaviour
{
    public void Click()
    {
        SceneManager.LoadScene(0);
    }
}
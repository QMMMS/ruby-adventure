﻿using UnityEngine;

/// <summary>
/// 草莓
/// </summary>
public class Collectble : MonoBehaviour
{
    public AudioClip collectedClip;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController pc = collision.GetComponent<PlayerController>();
        if (pc == null) return;//与玩家碰撞
        //print("player has touched straberry!");

        if (pc.MyCurrentHealth < pc.MyMaxHealth)
        {
            pc.ChangeHealth(1);
            Destroy(this.gameObject);//消失
            pc.PlaySound(collectedClip);
        }
    }
}

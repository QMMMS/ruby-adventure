﻿using UnityEngine;
/// <summary>
/// 伤害陷阱
/// </summary>
public class DamageArea : MonoBehaviour
{

    //玩家碰到这个区域时，血量 -1
    private void OnTriggerStay2D(Collider2D collision)
    {
        PlayerController pc = collision.GetComponent<PlayerController>();
        if (pc == null) return;
        //print("player has touched DamageArea!");
        pc.ChangeHealth(-1);
    }

}

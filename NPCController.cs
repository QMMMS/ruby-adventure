﻿using UnityEngine;

public class NPCController : MonoBehaviour
{
    public float startDisplayTime = 4.0f;
    private float startDisplayTimer;
    public float infoDisplayTime = 10.0f;
    private float infoDisplayTimer;
    public float thankDisplayTime = 4.0f;
    private float thankDisplayTimer;
    public GameObject startDialogBox;
    public GameObject infoDialogBox;
    public GameObject thankDialogBox;

    void Start()
    {
        startDialogBox.SetActive(false);
        infoDialogBox.SetActive(false);
        thankDialogBox.SetActive(false);
        startDisplayTimer = -1.0f;
        infoDisplayTimer = -1.0f;
        thankDisplayTimer = -1.0f;
    }

    void Update()
    {
        if (startDisplayTimer >= 0)
        {
            startDisplayTimer -= Time.deltaTime;
            if (startDisplayTimer < 0)
            {
                startDialogBox.SetActive(false);
                infoDisplayTimer = infoDisplayTime;
                infoDialogBox.SetActive(true);
            }
        }
        if (infoDisplayTimer >= 0)
        {
            infoDisplayTimer -= Time.deltaTime;
            if (infoDisplayTimer < 0) infoDialogBox.SetActive(false);
        }
        if (thankDisplayTimer >= 0)
        {
            thankDisplayTimer -= Time.deltaTime;
            if (thankDisplayTimer < 0) thankDialogBox.SetActive(false);
        }
    }

    public void DisplayDialog()
    {
        if (startDisplayTimer > 0 || infoDisplayTimer > 0 || thankDisplayTimer > 0) return;
        if (Counter.Instance.enermyLeft > 0)
        {
            startDisplayTimer = startDisplayTime;
            startDialogBox.SetActive(true);
        }
        else if (Counter.Instance.enermyLeft == 0)
        {
            thankDisplayTimer = thankDisplayTime;
            thankDialogBox.SetActive(true);
        }

    }
}

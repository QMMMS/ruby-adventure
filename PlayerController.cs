﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 角色控制
/// </summary>
public class PlayerController : MonoBehaviour
{
    #region MoveData
    public float standardSpeed = 5f;//标准速度
    private float horizontal;
    private float vertical;

    public float speedUpTime = 0.1f;
    private float speedUpTimer;
    private bool isSpeedUp;//加速
    #endregion MoveData

    #region HealthData
    private int maxHealth = 5;
    private int currentHealth;

    public int MyCurrentHealth { get { return currentHealth; } }//指定了“get”操作，因此它是只读的
    public int MyMaxHealth { get { return maxHealth; } }
    #endregion HealthData

    #region InvincibleData
    public float invincibleTime = 2f;//无敌时间
    private float invincibleTimer;
    private bool isInvincible;
    #endregion InvincibleData

    #region LaunchData
    public float launchingTime = 0.3f;//发射准备时间
    private float launchingTimer;
    private bool isLaunching;
    #endregion LaunchData

    #region AnimatorData
    private Vector2 lookDirection;
    private Animator animator;
    #endregion AnimatorData

    #region AudioData
    AudioSource audioSource;
    public AudioClip hitAudioClip;
    public AudioClip launchAudioClip;
    public AudioClip winAudioClip;
    #endregion

    #region DialogueData
    public float displayTime = 8.0f;
    private float displayTimer;
    public GameObject beginingDialogBox;
    public GameObject endDialogBox;
    private bool isEnd;
    #endregion DialogueData

    Rigidbody2D rigidbody2d;
    public GameObject bulletPrefab;

    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        currentHealth = maxHealth;
        invincibleTimer = 0;
        isInvincible = false;
        launchingTimer = 0;
        isLaunching = false;
        speedUpTimer = speedUpTime;
        isSpeedUp = false;
        lookDirection = new Vector2(1, 0);
        audioSource = GetComponent<AudioSource>();
        beginingDialogBox.SetActive(true);
        endDialogBox.SetActive(false);
        displayTimer = displayTime;
        isEnd = false;
    }

    void FixedUpdate()
    {
        #region MoveControl
        Vector2 position = rigidbody2d.position;//为了防止抖动，改成由刚体提供位置
        Vector2 direction = new Vector2(horizontal, vertical);
        float speed = standardSpeed;
        if (isSpeedUp) speed *= 2;

        position += speed * Time.deltaTime * direction;//无论游戏渲染的帧数如何，以相同速度奔跑
        rigidbody2d.MovePosition(position);
        #endregion MoveControl
    }

    void Update()
    {
        #region MoveControl
        //用户输入应该在Update函数处理
        horizontal = Input.GetAxisRaw("Horizontal");//A -> -1    D -> 1
        vertical = Input.GetAxisRaw("Vertical");//S -> -1    W -> 1
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (speedUpTimer > 0)
            {
                isSpeedUp = true;//加速控制
                speedUpTimer -= Time.deltaTime;//Time 类的静态成员
                if (speedUpTime < 0) speedUpTime = 0;
            }
            else isSpeedUp = false;
        }
        else
        {
            isSpeedUp = false;
            if (speedUpTimer < speedUpTime) speedUpTimer += Time.deltaTime * 0.4f;
        }
        UIPowerBar.instance.SetValue(speedUpTimer / speedUpTime);

        //对应非刚体代码
        //Vector2 pos = transform.position;
        //pos.x += moveX * sp * Time.deltaTime;
        //pos.y += moveY * sp * Time.deltaTime;
        //transform.position = pos;
        #endregion MoveControl

        #region InvincibleControl
        if (isInvincible)
        {
            invincibleTimer -= Time.deltaTime;
            if (invincibleTimer < 0) isInvincible = false;
        }
        #endregion InvincibleControl

        #region AnimatorControl
        Vector2 move = new Vector2(horizontal, vertical);
        //使用 Mathf.Approximately 而不是 ==，这是因为计算机存储浮点数的方式意味着精度会有很小的损失。
        //Approximately 会考虑这种不精确性，如果除去这种不精确性可以认为数值相等，将返回 true。  
        if (!Mathf.Approximately(move.x, 0.0f) || !Mathf.Approximately(move.y, 0.0f))
        {
            lookDirection.Set(move.x, move.y);
            lookDirection.Normalize();//从而使长度等于 1
            //通常，你将会归一化用于存储方向的向量，因为这时的长度并不重要，方向才重要。
            //注意：千万不要归一化用于存储位置的向量，因为这种向量更改 x 和 y 时会更改位置！  
        }

        animator.SetFloat("Look X", lookDirection.x);
        animator.SetFloat("Look Y", lookDirection.y);
        animator.SetFloat("Speed", move.magnitude);//长度
        #endregion AnimatorControl

        #region LaunchControl
        if (isLaunching)
        {
            launchingTimer -= Time.deltaTime;
            if (launchingTimer < 0) isLaunching = false;
        }
        else if (!isLaunching && Input.GetAxis("Fire1") == 1f)
        {
            isLaunching = true;
            launchingTimer = launchingTime;
            animator.SetTrigger("Launch");
            Vector2 realPosition = new Vector2(rigidbody2d.position.x, rigidbody2d.position.y + 0.5f);

            //Instantiate 的第一个参数是一个对象，在第二个参数的位置处创建一个副本，第三个参数是旋转。
            //Quaternion.identity 表示“无旋转”。
            GameObject bullet = Instantiate(bulletPrefab, realPosition, Quaternion.identity);
            BulletController bulletController = bullet.GetComponent<BulletController>();
            if (bulletController != null) bulletController.Launch(lookDirection, 300);
            PlaySound(launchAudioClip);
        }
        #endregion LaunchControl

        #region DialogueControl
        if (displayTimer >= 0)
        {
            displayTimer -= Time.deltaTime;
            if (displayTimer < 0)
            {
                beginingDialogBox.SetActive(false);
                endDialogBox.SetActive(false);
            }
        }


        if (Input.GetKeyDown(KeyCode.F))
        {
            //起点,方向,最大距离,图层遮罩
            RaycastHit2D hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, lookDirection, 3f, LayerMask.GetMask("NPC"));
            if (hit.collider != null)
            {
                NPCController NPC = hit.collider.GetComponent<NPCController>();
                if (NPC != null) NPC.DisplayDialog();
            }

            hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, lookDirection, 3f, LayerMask.GetMask("Enermy"));
            if (hit.collider != null)
            {
                EnermyController enermy = hit.collider.GetComponent<EnermyController>();
                if (enermy != null) enermy.DisplayDialog();
            }
        }
        #endregion DialogueControl

        #region EndControl
        if (!isEnd && Counter.Instance.enermyLeft == 0)
        {
            isEnd = true;
            endDialogBox.SetActive(true);
            displayTimer = displayTime;
            PlaySound(winAudioClip);
        }

        #endregion EndControl
    }


    public void ChangeHealth(int amount)
    {
        if (amount < 0)
        {
            if (isInvincible) return;
            isInvincible = true;
            invincibleTimer = invincibleTime;
            animator.SetTrigger("Hit");
            PlaySound(hitAudioClip);
            rigidbody2d.AddForce(-1 * 3000 * lookDirection);
        }
        //钳制功能可确保第一个参数绝不会小于第二个参数，也绝不会大于第三个参数
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);
        //print("Now health " + currentHealth);
        if (currentHealth == 0) SceneManager.LoadScene(0);
        UIHealthBar.Instance.SetValue(currentHealth / (float)maxHealth);
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}

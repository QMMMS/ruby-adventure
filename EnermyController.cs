﻿using UnityEngine;

/// <summary>
/// 敌人控制
/// </summary>
public class EnermyController : MonoBehaviour
{
    public float speed = 2f;
    public int numLeft = 5;

    #region ChangeDirectionData
    public bool isVertical;
    private Vector2 moveDirection;
    public float changeDirectionTime = 2f;
    private float changeDirectionTimer;//改变方向计时器
    #endregion ChangeDirectionData

    #region DisplayData
    public float displayTime = 4.0f;
    private float displayTimer;
    public GameObject badDialogBox;
    public GameObject goodDialogBox;
    #endregion DisplayData

    #region AudioData
    AudioSource audioSource;
    public AudioClip hitAudioClip;
    #endregion AudioData

    #region ParticleData
    public ParticleSystem smokeEffect;
    public ParticleSystem hitEffect;
    #endregion ParticleData

    bool isFixed;
    Rigidbody2D rigidbody2d;
    private Animator animator;

    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        moveDirection = isVertical ? Vector2.up : Vector2.right;
        changeDirectionTimer = changeDirectionTime;
        animator = GetComponent<Animator>();
        goodDialogBox.SetActive(false);
        badDialogBox.SetActive(false);
        displayTimer = -1.0f;
        audioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        if (isFixed) return;

        #region MoveControl
        Vector2 position = rigidbody2d.position;
        position.x += moveDirection.x * speed * Time.deltaTime;
        position.y += moveDirection.y * speed * Time.deltaTime;
        rigidbody2d.MovePosition(position);
        #endregion MoveControl
    }

    void Update()
    {
        #region DisplayControl
        if (displayTimer >= 0)
        {
            displayTimer -= Time.deltaTime;
            if (displayTimer < 0)
            {
                goodDialogBox.SetActive(false);
                badDialogBox.SetActive(false);
            }
        }
        #endregion DisplayControl

        if (isFixed) return;

        #region ChangeDirectionControl
        changeDirectionTimer -= Time.deltaTime;
        if (changeDirectionTimer < 0)
        {
            moveDirection *= -1;
            changeDirectionTimer = changeDirectionTime;
        }
        #endregion ChangeDirectionControl

        #region AnimatorControl
        animator.SetFloat("MoveX", moveDirection.x);
        animator.SetFloat("MoveY", moveDirection.y);
        #endregion AnimatorControl
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (isFixed) return;
        PlayerController pc = collision.gameObject.GetComponent<PlayerController>();
        if (pc == null) return;//与玩家碰撞
        pc.ChangeHealth(-1);
    }

    public void Fix()
    {
        isFixed = true;
        smokeEffect.Stop();
        audioSource.Stop();
        PlaySound(hitAudioClip);
        //Destroy(smokeEffect.gameObject);这种方法会使粒子瞬间消失

        //将 Rigidbody2d 的 simulated 属性设置为 false。这样会将刚体从物理系统模拟中删除
        //因此系统不会将机器人视为碰撞对象，并且修好的机器人不会再阻止飞弹，也不能伤害主角。
        rigidbody2d.simulated = false;
        animator.SetTrigger("Fixed");
        Instantiate(hitEffect, transform.position, Quaternion.identity);
        Counter.Instance.ChangeValue(-1);
    }

    public void DisplayDialog()
    {
        displayTimer = displayTime;
        if (isFixed) goodDialogBox.SetActive(true);
        else badDialogBox.SetActive(true);
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}

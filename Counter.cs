﻿using UnityEngine;

public class Counter : MonoBehaviour
{
    public static Counter Instance { get; set; }
    public int enermyLeft = 5;

    void Awake()
    {
        Instance = this;
    }

    public void ChangeValue(int value)
    {
        enermyLeft += value;
    }
}

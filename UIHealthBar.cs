﻿using UnityEngine;
using UnityEngine.UI;

public class UIHealthBar : MonoBehaviour
{
    //单例,可以直接引用场景中的生命值条脚本,静态成员在脚本的所有实例之间共享
    public static UIHealthBar Instance { get; private set; }

    public Image mask;//using UnityEngine.UI;
    private float originalSize;

    void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        //获取屏幕上的大小
        originalSize = mask.rectTransform.rect.width;
    }

    public void SetValue(float value)
    {
        //代码中设置大小和锚点
        mask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalSize * value);
    }
}

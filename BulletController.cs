﻿using UnityEngine;

public class BulletController : MonoBehaviour
{
    Rigidbody2D rigidbody2d;

    //在创建对象时（调用 Instantiate 时）就会立即调用 Awake
    //实例化 (Instantiate)，不调用 Start
    private void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        Destroy(this.gameObject, 2f);//2 秒后销毁
    }

    public void Launch(Vector2 moveDirection, float moveFroce)
    {
        rigidbody2d.AddForce(moveDirection * moveFroce);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        EnermyController enermy = collision.gameObject.GetComponent<EnermyController>();
        if (enermy != null) enermy.Fix();
        Destroy(this.gameObject);
    }
}
